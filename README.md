## EDA thermal power engineering


Проект по разведочному анализу(EDA) по теплоэнергетике России.

## В ходе данного проекта


* Был произведен EDA на языке программирования Python в jupyter notebook;
* Использован коррелиционный анализ и математическая статистика;
* Отрисован дашборд в Tableau(https://public.tableau.com/views/Analysiscostofheatsupply/Dashboard1?:language=en-US&publish=yes&:display_count=n&:origin=viz_share_link);
* Рассчитаны метрики для оценки работы теплоэнергетики в России


## Стэк


* Python == 3.8
* Tableau
* jupiter notebook